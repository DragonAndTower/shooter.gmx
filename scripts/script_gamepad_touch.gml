var cursor_direction = argument0;

if (cursor_direction == 0 || cursor_direction == 8) {
    with (global.player) {
        event_perform(ev_keyboard, vk_right);
    }
} else if (cursor_direction == 1) {
    with (global.player) {
        event_perform(ev_keyboard, vk_right);
        event_perform(ev_keyboard, vk_up);
    }
} else if (cursor_direction == 2) {
    with (global.player) {
        event_perform(ev_keyboard, vk_up);
    }
} else if (cursor_direction == 3) {
    with (global.player) {
        event_perform(ev_keyboard, vk_left);
        event_perform(ev_keyboard, vk_up);
    }
} else if (cursor_direction == 4) {
    with (global.player) {
        event_perform(ev_keyboard, vk_left);
    }
} else if (cursor_direction == 5) {
    with (global.player) {
        event_perform(ev_keyboard, vk_left);
        event_perform(ev_keyboard, vk_down);
    }
} else if (cursor_direction == 6) {
    with (global.player) {
        event_perform(ev_keyboard, vk_down);
    }
} else if (cursor_direction == 7) {
    with (global.player) {
        event_perform(ev_keyboard, vk_right);
        event_perform(ev_keyboard, vk_down);
    }
}        
