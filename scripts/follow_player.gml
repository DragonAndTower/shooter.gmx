if (instance_exists(global.player)) {
    direction = radtodeg(
        arctan2(
            - global.player.y + y,
            global.player.x - x
        )
    );
}
