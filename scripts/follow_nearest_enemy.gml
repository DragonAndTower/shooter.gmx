if (!target or !(instance_exists(target)) or !(target.active) ) {
    target = instance_nearest(x, y, obj_enemy_base);
}

if (target and instance_exists(target)) {
   var angle = arctan2((-target.y + y) , (target.x - x));
   var current = degtorad(direction);
   var delta = radtodeg(angle - current);
   
   while (delta > 180) {
     delta = delta - 360;
   }

    while (delta < -180) {
     delta = delta + 360;
   }   
   
   var absdelta = abs(delta);
   
   if (absdelta > 5) {
     absdelta = 5;
   }
   
   if (delta < 0) {
     absdelta = -absdelta;
     }
     
   direction = direction + absdelta;
}
