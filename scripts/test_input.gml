for (var i = 0; i < 5; i += 1) {

    if (device_mouse_check_button_pressed(i, mb_left)) { 
        if (device_mouse_x(i) < (view_xview + (view_wview / 2))) {
            with (global.gamepad) {
                script_execute(script_gamepad_update, ev_left_press, i);
            }
        } else {
           with (global.gamebuttons) {
                script_execute(script_gamepad_update, ev_left_press, i);
            }
        }
    }
    
    if (device_mouse_check_button_released(i, mb_left)) { 
        with (global.gamepad) {
            script_execute(script_gamepad_update, ev_left_release, i);
        }
        with (global.gamebuttons) {
            script_execute(script_gamepad_update, ev_left_release, i);
        }
    }
    
    if (device_mouse_check_button_pressed(i, mb_right)) { 
        event_perform(ev_keyboard, vk_enter);
    }
}
