if (global.player and instance_exists(global.player)) {
    if (!target_set) {
        show_debug_message('Going behind you!!');
        target_x = global.player.x;
        target_y = global.player.y;
        
        direction = radtodeg(arctan2(-(target_y - y), (target_x - x)));
        speed = 12;
           
        target_set = true;
        on_target = false;
        bullet = null;
    }   
}

if (on_target and !bullet) {
    show_debug_message('On target, shoot!!');
    bullet = instance_create(x,y,obj_enemy_bullet_slash);
    
    if (global.player and instance_exists(global.player)) {    
        direction = radtodeg(arctan2(-(global.player.y - y), (global.player.x - x)));
    }
    
    bullet.direction = direction;
}

if (target_set and on_target and (alarm[10] <= 0)) {
    show_debug_message('Find again...');
    target_set = false;
    on_target = false;
}   

if (target_set) {
    if ((distance_to_point(target_x, target_y) < speed/2) and !on_target) {
        show_debug_message('On target! Wait...');
        speed = 0;
        alarm_set(10, 30);
        on_target = true;
    }
}

